function moviList(url) {
    return fetch(url)
        .then(response => {
            if (!response) {
                throw new Error("Error 1");
            }
            return response.json();
        });
}

const url = "https://ajax.test-danit.com/api/swapi/films";

function renderFilms(films) {
    const filmList = document.getElementById("filmList");
    const charactersList = document.getElementById("charactersList");
    const spinner = document.querySelector(".spinner");

    films.forEach(async film => {
        const filmTab = document.createElement("tr");
        filmTab.innerHTML = `
            <td>${film.episodeId}</td>
            <td>${film.name}</td>
            <td>${film.openingCrawl}</td>
        `;
        filmList.append(filmTab);
                const characters = await Promise.all(film.characters.map(moviList));

                charactersList.style.display = "block";
                spinner.style.display = "none";

                characters.forEach(character => {
                    const characterTab = document.createElement("tr");
                    characterTab.innerHTML = `
                        <td>${character.name}</td>
                        <td>${character.gender}</td>
                        <td>${character.hairColor}</td>
                        <td>${character.height}</td>
                    `;
                    charactersList.append(characterTab);
                });
    });
}

moviList(url)
    .then(renderFilms)
    .catch(error => console.error("Error 3", error));
